from memory_profiler import profile
from timebudget import timebudget

from common import get_etalon
from schearch import get_word_positions

seq = get_etalon("example/etalon.seq")

@profile
def calc():
    for word in set(seq):
        word_positions = get_word_positions(word, seq)


with timebudget("Calculated sequence"):
    calc()