class SchearchParser:

    def __init__(self):
        self.is_finish = False
        self.current_sequence = ''
        self.source_sequence = []
        self.repeatable_subsequences = []

    def parse(self, seq):
        self.source_sequence = seq[1:]
        self._calc()
        result = [seq[0] + x for x in self.repeatable_subsequences]
        return result

    def _calc(self, i=0, to_remove_parent=False):
        if not len(self.source_sequence):
            return

        cursor = self.source_sequence[i]

        if to_remove_parent:
            cursor.pop(0)

        if not len(cursor):
            self.is_finish = not to_remove_parent
            self.source_sequence.remove(cursor)
            if self.is_finish:
                self.repeatable_subsequences.append(self.current_sequence)
                self.is_finish = True
            self.current_sequence = self.current_sequence[:-1]

            return self._calc(i - 1, to_remove_parent=True)

        for item in cursor:
            self.current_sequence += item

            r = self._calc(i + 1)
