def get_etalon(filename="etalon.seq"):
    with open(filename, "r") as f:
        return f.read()


def hash_list(l):
    return hash(''.join(l))
