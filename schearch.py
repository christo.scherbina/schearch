import itertools
import re

from memory_profiler import profile
from timebudget import timebudget
from collections import defaultdict

def get_word_positions(word, seq):
    result = [m.start() for m in re.finditer(word, seq)]
    return result

class Schearch:

    def __init__(self, seq):
        self.seq = seq
        self.seq_len = len(seq)
    @profile
    def calc(self):
        def _(word):
            # Получение позиций всех повторений слова
            positions = get_word_positions(word, self.seq)
            # todo: Try iterate recursion
            return self._find(positions, [word])
        
        result = [_(w) for w in set(self.seq)]
            
        return result


    
    #def _call(self, word_positions):
        
        

    def _find(self, word_positions, result):
        # Получить цепочки встречаемости слова в последовательности
        chains = self._get_chains(word_positions)
        
        result.append(list(chains.keys()))
        for next_word in chains:
            next_positions = chains[next_word]
            result = self._find(next_positions, result)

        return result

    def _get_chains(self, word_positions):
        chains = defaultdict(list)
        
        def copy_word(index):
            word = self.seq[index]
            chains[word].append(index)
            
        word_positions = [w for w in word_positions if w + 1 != self.seq_len]
        for p in word_positions:
            copy_word(p + 1)

        # Убрать слова без дубликатов
        chains = {k: v for (k, v) in chains.items() if len(v) > 1}

        return chains

