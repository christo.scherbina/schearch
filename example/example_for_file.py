import json
import random

from schearch import Schearch
from schearch_parser import SchearchParser


def _check_file_is_exist(filename):
    import os
    path = f'./{filename}'
    pred = lambda p: os.path.isfile(p) and os.access(p, os.R_OK)
    return pred(path)


def _create_file(filename, size):
    with open(filename, 'w+') as f:
        data = ''.join(str(random.randint(0, 9)) for x in range(size))
        f.write(data)


def _read_file(filename):
    with open(filename, 'r') as f:
        return f.read()


def _flush_result(filename, seq):
    with open(f'{filename}.result', 'w') as fo:
        json.dump(seq, fo)


def get_or_create_file(name="example_schearch", size=1000):
    filename = f'{name}_{size // 1000}k.seq'

    if not _check_file_is_exist(filename):
        _create_file(filename, size)

    f = open(filename, 'r')
    return f


with get_or_create_file(size=100000) as _f:
    seq = _f.read()
    non_parsed_sequence = Schearch(seq).calc()
    _flush_result(_f.name, non_parsed_sequence)
    for _ in non_parsed_sequence:
        sp = SchearchParser()
        match = sp.parse(_)
