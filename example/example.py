from timebudget import timebudget

from common import get_etalon, hash_list
from schearch import Schearch
from schearch_parser import SchearchParser


# warn: PYTHONHASHSEED=0 for hash


def get_repeated_words():
    seq = get_etalon()
    non_parsed_sequence = Schearch(seq).calc()
    parser = SchearchParser()

    return list(map(parser.parse, non_parsed_sequence))


# for 1k min 37ms max 60ms, avg 42

with timebudget("Calculated sequence"):
    repeated_words = get_repeated_words()
flat_list = [item for sublist in repeated_words for item in sublist]

pre_calc_hash_result = -985778745

assert pre_calc_hash_result == hash_list(flat_list)
